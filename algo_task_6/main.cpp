// Мазырин
// Роман

/*

На вокзале есть некоторое количество тупиков, куда прибывают электрички. Этот вокзал является их конечной станцией.
Дано расписание движения электричек, в котором для каждой электрички указано время ее прибытия, а также время
отправления в следующий рейс. Электрички в расписании упорядочены по времени прибытия. Когда электричка прибывает,
ее ставят в свободный тупик с минимальным номером. При этом если электричка из какого-то тупика отправилась в момент времени X,
то электричку, которая прибывает в момент времени X, в этот тупик ставить нельзя, а электричку, прибывающую в момент X+1 — можно.
В данный момент на вокзале достаточное количество тупиков для работы по расписанию.

Напишите программу, которая по данному расписанию определяет, какое минимальное количество тупиков требуется для работы вокзала.
Формат входных данных. Вначале вводится n - количество электричек в расписании. Затем вводится n строк для каждой электрички,
в строке - время прибытия и время отправления. Время - натуральное число от 0 до 10^9. Строки в расписании упорядочены по времени
прибытия.

Формат выходных данных. Натуральное число - минимальное количеством тупиков.

Максимальное время: 50мс, память: 5Мб.

 */

#include <iostream>

using namespace std;

struct timetable {
    long int timeArrive;
    long int timeToGo;
    timetable() {};
    timetable(long int ta, long int tg) : timeArrive(ta), timeToGo(tg) {}
};

class Heap {
private:
    timetable* buffer;
    int numOfDeadlocks;
    int bufferSize;
public:
    Heap() {
        bufferSize = 1;
        buffer = new timetable[1];
        numOfDeadlocks = 0;
    }
    ~Heap() {
        delete [] buffer;
    }

    void grow() {
        timetable* temp = new timetable[bufferSize*2];
        for( int i = 0; i < bufferSize; i++ ){
            temp[i] = buffer[i];
        }
        bufferSize *= 2;
        delete [] buffer;
        buffer = temp;
    }

    long int getTheMostEarlyTimeToGo() {
        if( numOfDeadlocks > 0 ) {
            return buffer[0].timeToGo;
        }
        return 0;
    }

    void swap(int elemKey1, int elemKey2) {
        timetable temp = buffer[elemKey1];
        buffer[elemKey1] = buffer[elemKey2];
        buffer[elemKey2] = temp;
    }

    void SiftUp(int elemKey) {
        if( elemKey == 0 ) {
            return;
        }
        int parent = (elemKey-1)/2;
        if( buffer[parent].timeToGo > buffer[elemKey].timeToGo ) {
            swap(parent, elemKey);
        }
        SiftUp(parent);
    }

    void SiftDown(int elemKey) {
        int leftChild = elemKey*2+1;
        int rightChild = elemKey*2+2;
        int largest = elemKey;

        if( leftChild < numOfDeadlocks && buffer[leftChild].timeToGo < buffer[elemKey].timeToGo ) {
            largest = leftChild;
        }
        if( rightChild < numOfDeadlocks && buffer[rightChild].timeToGo < buffer[largest].timeToGo ) {
            largest = rightChild;
        }
        if( largest != elemKey ) {
            swap(largest, elemKey);
            SiftDown(largest);
        }
    }
    timetable getTheMostEarly() {
        if( numOfDeadlocks == 0 ){
            return timetable(-1, -1);
        }
        timetable res = buffer[0];
        swap(0, numOfDeadlocks-1);
        numOfDeadlocks--;
        SiftDown(0);
        return res;
    }

    void addElem(timetable t) {
        if( numOfDeadlocks == 0 ) {
            buffer[0] = t;
            numOfDeadlocks++;
            return;
        }

        if( getTheMostEarlyTimeToGo() < t.timeArrive ) {
            buffer[0] = t;
            SiftDown(0);
            return;
//            getTheMostEarly();
//            buffer[numOfDeadlocks++] = t;
//            SiftUp(numOfDeadlocks-1);
//            return;
        }

        if( numOfDeadlocks == bufferSize ) {
            grow();
        }

        buffer[numOfDeadlocks++] = t;
        SiftUp(numOfDeadlocks-1);

    }

    int getNumOfDeadlocks() {
        return numOfDeadlocks;
    }

};

void input(Heap&);

int main() {
    Heap H;
    input(H);
    cout << H.getNumOfDeadlocks();
    return 0;
}

void input(Heap& H) {
    int N = 100;
    cin >> N;

    long int TimeArrive = 0;
    long int TimeToGo = 0;

    for( int i = 0; i < N; i++ ) {
        cin >> TimeArrive;
        cin >> TimeToGo;
//        H.addElem(timetable((i+1)*10, 100 + i*10));
        H.addElem(timetable(TimeArrive, TimeToGo));
    }
}