// Мазырин
// Роман

/*
 “Считалочка”. В круг выстроено N человек, пронумерованных числами от 1 до N.
 Будем исключать каждого k-ого до тех пор, пока не уцелеет только один человек.
 (Например, если N=10, k=3, то сначала умрет 3-й, потом 6-й, затем 9-й, затем 2-й, затем 7-й,
 потом 1-й, потом 8-й, за ним - 5-й, и потом 10-й. Таким образом, уцелеет 4-й.)
 Необходимо определить номер уцелевшего.
 */

#include <iostream>
#include "assert.h"

using namespace std;

void killAllK( bool[], int, int );

int main() {

    int length = 0;
    int k = 0;

    cin >> length;
    assert( length > 0 );
    cin >> k;

    bool* mas = new bool[length+1];
    for( int i = 0; i <= length; i++ ) {
        mas[i] = true;
    }

    killAllK( mas, k, length );

    for( int i = 1; i <= length; i++ ) {
        if( mas[i] ) {
            cout << i;
        }
    }

    delete [] mas;
    return 0;
}

void killAllK( bool mas[], int k, int length ) {
    int haveDestroyed = 0;
    int i = 0;
    int iter = 0; // Счетчик, сколько еще осталось пройти цифр которые не вычеркнуты
    while( haveDestroyed < length-1 ) {
        iter = k;
        while( iter > 0 ) {
            i++;
            if( i > length ) {
                i -= (length);
            }
            if( mas[i] ) {
                iter--;
            }
        }
        mas[i] = false;
        haveDestroyed++;
    }
}