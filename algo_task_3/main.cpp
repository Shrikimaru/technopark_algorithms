// Мазырин
// Роман

/*
 Даны два массива неповторяющихся целых чисел, упорядоченные по неубыванию.
 A[0..n-1] и B[0..m-1]. n >> m. Найдите их пересечение.
 Требуемое время работы: O(m * log k), где k - позиция элементта B[m-1] в массиве A.
 В процессе поиска очередного элемента B[i] в массиве A пользуйтесь результатом
 поиска элемента B[i-1].
*/

#include <iostream>
#include <assert.h>

using namespace std;

int unionArrays( int*, int, int*, int, int* );
int binnarySearch(int*, int, int, int);

int main() {
    int lengthA = 0;
    int lengthB = 0;
    cin >> lengthA;
    assert( lengthA > 0 );
    cin >> lengthB;
    assert( lengthB > 0 );

    int* A = new int[lengthA];
    int* B = new int[lengthB];
    int* result = new int[lengthB];

    for( int i = 0; i < lengthA; i++ ) {
        cin >> A[i];
    }
    for( int i = 0; i < lengthB; i++ ) {
        cin >> B[i];
    }

    int lengthRes = unionArrays( A, lengthA, B, lengthB, result);

    for( int i = 0; i < lengthRes; i++ ) {
        cout << result[i] << " ";
    }

    delete [] A;
    delete [] B;
    delete [] result;

    return 0;
}

int unionArrays( int* arrA, int lengthA, int* arrB, int lengthB, int* result ) {

    int lengthRes = 0;

    if( arrA[0] > arrB[lengthB-1] ) {
        return 0;
    }

    int lb = 1;
    int rb = 2;

    for( int i = 0; i < lengthB; i++ ) {

        while(  rb < lengthA &&
                arrA[rb] <= arrB[i]) {
            rb *= 2;
            lb *= 2;
        }

        if( rb > lengthA ) {
            rb = lengthA;
        }

        int keyInA = binnarySearch(arrA, lb-1, rb, arrB[i]);
        if( keyInA != -1 ) {
            result[lengthRes] = arrA[keyInA];
            lengthRes++;
            lb = keyInA;
            rb *= 2;
        }

    }

    return lengthRes;

}

int binnarySearch(int* arr, int lb, int rb, int val) {
    int mid = 0;
    do {
        mid = (lb + rb)/2;
        if( val < arr[mid] ) {
            rb = mid - 1;
        }
        else {
            if( val > arr[mid] ) {
                lb = mid + 1;
            } else {
                return mid;
            }
        }
        if( lb > rb ) {
            return -1;
        }
    } while( 1 );
}