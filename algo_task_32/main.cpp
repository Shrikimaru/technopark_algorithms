// Мазырин
// Роман
/*
 Дано число N < 106 и последовательность целых чисел из [-231..231] длиной N.
Требуется построить бинарное дерево, заданное наивным порядком вставки.
Т.е., при добавлении очередного числа K в дерево с корнем root, если root→Key ≤ K, то узел K добавляется в правое поддерево root; иначе в левое поддерево root.

2_3. Выведите элементы в порядке post-order (снизу вверх).
*/
#include <iostream>

using namespace std;

typedef long long int myType;

struct TreeNode {
    TreeNode* left;
    TreeNode* right;
    myType val;
    TreeNode(myType pVal) : left(0), right(0), val(pVal) {}
};

class BinaryTree {
private:
    TreeNode* rootNode;
public:
    BinaryTree() : rootNode(0) {}

    void insertVal(myType val) {
        if( rootNode == 0 ) {
            rootNode = new TreeNode(val);
            return;
        }
        TreeNode* newNode = rootNode;
        while( true ) {
            if( val <= newNode->val ) {
                if( newNode->left == 0 ) {
                    newNode->left = new TreeNode(val);
                    return;
                } else {
                    newNode = newNode->left;
                }
            } else {
                if( newNode->right == 0 ) {
                    newNode->right = new TreeNode(val);
                    return;
                } else {
                    newNode = newNode->right;
                }
            }
        }
    }

    void outputFromDownToUp() {
        output(rootNode);
    }

    void output(TreeNode* node) {
        if( node->left != 0 ) {
            output(node->left);
        }
        if( node->right != 0 ) {
            output(node->right);
        }
        cout << node->val << " ";
    }

    void DeleteNode(TreeNode*& node) {
        if( node->left != 0 ) {
            DeleteNode(node->left);
        }
        if( node->right != 0 ) {
            DeleteNode(node->right);
        }
        delete node;
    }

    ~BinaryTree() {
        DeleteNode(rootNode);
    }
};

int main() {
    myType count = 6;
    cin >> count;
    myType val = 0;
    BinaryTree* tree = new BinaryTree();
    for( int i = 0; i < count; i++ ) {
        cin >> val;
        tree->insertVal(val);
    }
    tree->outputFromDownToUp();

    delete tree;
    return 0;
}