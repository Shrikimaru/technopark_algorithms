// Мазырин
// Роман
/*
Задано N точек на плоскости. Указать (N-1)-звенную несамопересекающуюся замкнутую ломаную, проходящую через все эти точки.
Предполагается, что никакие три точки не лежат на одной прямой.
Указание: стройте ломаную от точки, имеющей наименьшую координату x.
Если таких точек несколько, то используйте точку с наименьшей координатой y.
Точки на ломаной расположите в порядке убывания углов лучей от начальной точки до всех остальных точек.
*/

#include <iostream>

using namespace std;

class Point;
float tg(Point& startPoint, Point& endPoint);
void findMin(Point* arr, int size);

class Point{
public:
    float x;

    float y;
    Point(){};

    Point(float px, float py) : x(px), y(py) {}
    static Point beginPoint;
    Point(const Point& p) {
        x = p.x;
        y = p.y;
    }
    bool operator>(Point& p) {
        return ( (tg(Point::beginPoint, *this) > tg(Point::beginPoint, p)) ||
                 (tg(Point::beginPoint, *this) == tg(Point::beginPoint, p) && this->x < p.x)
        );
    }
    bool operator<(Point& p) {
        return ( (tg(Point::beginPoint, *this) < tg(Point::beginPoint, p)) ||
                (tg(Point::beginPoint, *this) == tg(Point::beginPoint, p) && this->x > p.x)
        );
    }
//    bool operator==(Point& p) {
//        return (tg(Point::beginPoint, *this) == tg(Point::beginPoint, p));
//    }
};
Point Point::beginPoint = Point(0, 0);

float tg(Point& startPoint, Point& endPoint) {
    return ((endPoint.y - startPoint.y)/(endPoint.x - startPoint.x));
}

void heap_insert(Point* arr, int size, Point val){
    arr[size] = val;
    for( int k = size; k > 0; k /= 2) {
        if( arr[k] > arr[k/2]) {
            swap(arr[k], arr[k/2]);
        }
//        if( arr[k] == arr[k/2] ) {
//            if( arr[k].x < arr[k/2].x ) {
//                swap(arr[k], arr[k/2]);
//            }
//            break;
//        }
    }
}

Point heap_pop(Point* arr, int size){
    Point res = arr[size-1];
    swap(arr[0], arr[size-1]);
    int curKey = 0;
    while( curKey < size-1 ) {
        int leftChildKey = curKey*2+1;
        int rightChildKey = curKey*2+2;
        int largestChildKey = curKey;

        if( leftChildKey < size-1 &&
            arr[leftChildKey] > arr[curKey] ) {
            largestChildKey = leftChildKey;
        }

        if( rightChildKey < size-1 &&
            arr[rightChildKey] > arr[largestChildKey] ) {
            largestChildKey = rightChildKey;
        }

        if( curKey != largestChildKey ) {
            swap(arr[curKey], arr[largestChildKey]);
            curKey = largestChildKey;
        } else {
            break;
        }
    }
    return res;
}

void heap_sort(Point* arr, int size) {
    Point* buff = new Point[size]();
    for (int i = 0; i < size; ++i) {
        heap_insert(buff, i, arr[i]);
    }
    for (int i = 0; i < size; ++i) {
        arr[i] = buff[0];
        heap_pop(buff, size-i);
    }
    delete [] buff;
}

int main() {
    int numOfPoints = 0;
    cin >> numOfPoints;
    Point* p = new Point[numOfPoints];
    int x = 0;
    int y = 0;

    for( int i = 0; i < numOfPoints; i++ ) {
        cin >> x;
        cin >> y;
        p[i] = Point(x, y);
    }
    findMin(p, numOfPoints);
    heap_sort(p, numOfPoints-1);

    cout << p[numOfPoints].x << " " << p[numOfPoints].y << endl;
    for( int i = 0; i < numOfPoints-1; i++ ) {
        cout << p[i].x << " " << p[i].y << endl;
    }

    return 0;
}

void findMin(Point* arr, int size) {
    int minKey = 0;
    for( int i = 0; i < size; i++ ) {
        if( arr[i].x < arr[minKey].x ) {
            minKey = i;
        }
        if( arr[i].x == arr[minKey].x ) {
            if( arr[i].y < arr[minKey].y ) {
                minKey = i;
            }
        }
    }
    Point::beginPoint = arr[minKey];
    swap(arr[minKey], arr[size-1]);
}