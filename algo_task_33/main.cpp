// Мазырин
// Роман

/*
Дано число N < 106 и последовательность пар целых чисел из [-231..231] длиной N.
Построить декартово дерево из N узлов, характеризующихся парами чисел {Xi, Yi}.
Каждая пара чисел {Xi, Yi} определяет ключ Xi и приоритет Yi в декартовом дереве.
Добавление узла в декартово дерево выполняйте второй версией алгоритма, рассказанного на лекции:
При добавлении узла (x, y) выполняйте спуск по ключу до узла P с меньшим приоритетом.
Затем разбейте найденное поддерево по ключу x так, чтобы в первом поддереве все ключи меньше x,
а во втором больше или равны x. Получившиеся два дерева сделайте дочерними для нового узла (x, y).
Новый узел вставьте на место узла P.
Построить также наивное дерево поиска по ключам Xi методом из задачи 2.

3_1. Вычислить разницу глубин наивного дерева поиска и декартового дерева
 */

#include <iostream>

using namespace std;

typedef int myType;

struct TreeNode {
    TreeNode* left;
    TreeNode* right;
    myType val;
    TreeNode(myType pVal) : left(0), right(0), val(pVal) {}
};

class BinaryTree {
private:
    TreeNode* rootNode;
public:
    BinaryTree() : rootNode(0) {}

    void insertVal(myType val) {
        if( rootNode == 0 ) {
            rootNode = new TreeNode(val);
            return;
        }
        TreeNode* newNode = rootNode;
        while( true ) {
            if( val <= newNode->val ) {
                if( newNode->left == 0 ) {
                    newNode->left = new TreeNode(val);
                    return;
                } else {
                    newNode = newNode->left;
                }
            } else {
                if( newNode->right == 0 ) {
                    newNode->right = new TreeNode(val);
                    return;
                } else {
                    newNode = newNode->right;
                }
            }
        }
    }

    void outputFromDownToUp() {
        output(rootNode);
    }

    void output(TreeNode* node) {
        if( node->left != 0 ) {
            output(node->left);
        }
        if( node->right != 0 ) {
            output(node->right);
        }
        cout << node->val << " ";
    }

    int countMaxDeep() {
        return countDeep(rootNode, 0);
    }

    int countDeep(TreeNode* node, int depth) {
        if (node == 0) {
            return depth;
        }
        return max(countDeep(node->left, depth+1), countDeep(node->right, depth+1));
    }

    void DeleteNode(TreeNode*& node) {
        if( node->left != 0 ) {
            DeleteNode(node->left);
        }
        if( node->right != 0 ) {
            DeleteNode(node->right);
        }
        delete node;
    }

    ~BinaryTree() {
        DeleteNode(rootNode);
    }
};

struct DecartNode {
    myType key_x;
    myType priority_y;
    DecartNode* left;
    DecartNode* right;

    DecartNode(myType x, myType y) : key_x(x), priority_y(y), left(0), right(0) {}
};

class DecartTree {
private:

    DecartNode* rootNode;

    void splitByKey( DecartNode* currentNode, myType key, DecartNode*& left, DecartNode*& right ) {
        if( currentNode == 0 ) {
            left = 0;
            right = 0;
        } else if( currentNode->key_x <= key ) {
            splitByKey( currentNode->right, key, currentNode->right, right );
            left = currentNode;
        } else {
            splitByKey( currentNode->left, key, left, currentNode->left );
            right = currentNode;
        }
    }

    DecartNode* insertIntoPlace(DecartNode*& node, myType x, myType y) {
        DecartNode* newNode = new DecartNode(x, y);
        DecartNode* left = 0;
        DecartNode* right = 0;
        splitByKey(node, x, left, right);
        newNode->left = left;
        newNode->right = right;
        return newNode;
    }

public:

    DecartTree() : rootNode(0) {}

    void insertVal( myType x, myType y ) {
        if( rootNode == 0 ) {
            rootNode = new DecartNode(x, y);
            return;
        }

        if( rootNode->priority_y < y ) {
            rootNode = insertIntoPlace(rootNode, x, y);
            return;
        }

        DecartNode* tempNode = rootNode;
        bool notInserted = true;

        while( notInserted ) {
            if( x <= tempNode->key_x ) {
                if( tempNode->left == 0 ) {
                    tempNode->left = new DecartNode(x, y);
                    notInserted = false;
                } else {
                    if( tempNode->left->priority_y < y ) {
                        tempNode->left = insertIntoPlace(tempNode->left, x, y);
                        notInserted = false;
                    } else {
                        tempNode = tempNode->left;
                    }
                }
            } else {
                if( tempNode->right == 0 ) {
                    tempNode->right = new DecartNode(x, y);
                    notInserted = false;
                } else {
                    if( tempNode->right->priority_y < y ) {
                        tempNode->right = insertIntoPlace(tempNode->right, x, y);
                        notInserted = false;
                    } else {
                        tempNode = tempNode->right;
                    }
                }
            }
        }
    }

    int countMaxDeep() {
        return countDeep(rootNode, 0);
    }

    int countDeep(DecartNode* node, int depth) {
        if (node == 0) {
            return depth;
        }
        return max(countDeep(node->left, depth+1), countDeep(node->right, depth+1));
    }

    void outputFromDownToUp() {
        output(rootNode);
    }

    void output(DecartNode* node) {
        if( node->left != 0 ) {
            output(node->left);
        }
        if( node->right != 0 ) {
            output(node->right);
        }
        cout << node->key_x << " ";
    }

    void DeleteNode(DecartNode*& node) {
        if( node->left != 0 ) {
            DeleteNode(node->left);
        }
        if( node->right != 0 ) {
            DeleteNode(node->right);
        }
        delete node;
    }

    ~DecartTree() {
        DeleteNode(rootNode);
    }
};

int main() {
    int count = 0;
    cin >> count;
    myType key_x = 0;
    myType priority_y = 0;
    BinaryTree* tree = new BinaryTree();
    DecartTree* dTree = new DecartTree();
    for( int i = 0; i < count; i++ ) {
        cin >> key_x;
        cin >> priority_y;
        tree->insertVal(key_x);
        dTree->insertVal(key_x, priority_y);
    }

    cout << tree->countMaxDeep() - dTree->countMaxDeep();

    delete tree;
    delete dTree;
    return 0;
}