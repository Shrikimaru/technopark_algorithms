// Мазырин
// Роман

/*
Дана последовательность N прямоугольников различной ширины и высоты (wi,hi).

Прямоугольники расположены, начиная с точки (0, 0), на оси ОХ вплотную друг за другом (вправо).
Требуется найти M - площадь максимального прямоугольника (параллельного осям координат),
который можно вырезать из этой фигуры.

Время работы - O(n).

Формат входных данных. В первой строке задано число N (1 ≤ N ≤ 8000). Далее идет N строк.
В каждой строке содержится два числа: ширина и высота i-го прямоугольника

Формат выходных данных. вывести число M.

 */

#include <iostream>
#include <assert.h>

using namespace std;

struct Step {
    int width;
    int height;
    int beginX;

    Step() {};
    Step(int w, int h) : width(w), height(h) {};
};

class StackOfSteps {

private:

    Step* buffer;
    int bufferSize;
    int head;
    int maxSquare;

    void grow() {
        bufferSize *= 2;
        Step* temp = new Step[bufferSize];
        for( int i = 0; i < head; i++ ) {
            temp[i] = buffer[i];
        }
        delete [] buffer;
        buffer = temp;
    }

    void push(Step s) {
        if( bufferSize == head ) {
            grow();
        }
        buffer[head++] = s;
    }

    Step pop() {
        assert( head > 0 );
        Step res = buffer[--head];
        return res;
    }

public:

    StackOfSteps() {
        bufferSize = 1;
        buffer = new Step[bufferSize];
        head = 0;
        maxSquare = 0;
    }

    int getMaxSquare() {
        addNextSquare(Step(1, 0));
        return maxSquare;
    }

    void concatStepsToMaxSquare(Step s) {
        int endX = s.beginX + s.width;
        Step prevStep = pop();
        int lastEndX = prevStep.beginX+prevStep.width;
        int squareOfLastBigStep = prevStep.width * prevStep.height;

        if( squareOfLastBigStep > maxSquare ) {
            maxSquare = squareOfLastBigStep;
        }

        while( head > 0 && buffer[head-1].height >= s.height ) {
            prevStep = pop();
            squareOfLastBigStep = (lastEndX - prevStep.beginX) * prevStep.height;
            if( squareOfLastBigStep > maxSquare ) {
                maxSquare = squareOfLastBigStep;
            }
        }

        int beginXOfNewConcatedStep = buffer[head].beginX;
        Step newConcatedStep(endX - beginXOfNewConcatedStep, s.height);
        newConcatedStep.beginX = beginXOfNewConcatedStep;
        push(newConcatedStep);
    }

    void addNextSquare(Step s) {

        if( head == 0 ) {
            s.beginX = 0;
            int square = s.width * s.height;
            if( square > maxSquare ) {
                maxSquare = square;
            }
            push(s);
            return;
        } else {
            s.beginX = buffer[head-1].beginX + buffer[head-1].width;
        }

        if( s.height <= buffer[head-1].height ) {
            concatStepsToMaxSquare(s);
        } else {
            push(s);
        }

    }

    ~StackOfSteps() {
        delete [] buffer;
    }

};

void input(StackOfSteps& s, int&);

int main() {

    int result = 0;
    StackOfSteps s;

    input(s, result);

    cout << result;

    return 0;
}

void input(StackOfSteps& s, int& res) {

    int N = 4;
    cin >> N;

    int Width = 0;
    int Height = 0;

    for( int i = 0; i < N; i++ ) {
        cin >> Width;
        cin >> Height;
        s.addNextSquare( Step(Width, Height) );
    }

    res = s.getMaxSquare();
}