// Мазырин
// Роман
/*
Даны неотрицательные целые числа n,k и массив целых чисел из [0..10^9] размера n.
Требуется найти k-ю порядковую статистику. т.е. напечатать число, которое бы стояло
на позиции с индексом k (0..n-1) в отсортированном массиве. Напишите нерекурсивный алгоритм.
Требования к дополнительной памяти: O(n). Требуемое среднее время работы: O(n).
Функцию Partition следует реализовывать методом прохода двумя итераторами в одном направлении.
Реализуйте стратегию выбора опорного элемента “медиана трёх”.
Функцию Partition реализуйте методом прохода двумя итераторами от начала массива к концу.
 */
#include <iostream>

using namespace std;

int findMedianIndex(int* arr, int left, int right);
int partition(int*arr, int left, int right);
int findKStatistic(int* arr, int left, int right, int k);
void input(int* arr, int size);
void output(int* arr, int size);
int findMin(int* arr, int size);
int findMax(int* arr, int size);

int main() {
    int size = 0;
    int k = 0;
    cin >> size;
    cin >> k;
    int* arr = new int[size];
//    int arr[8] = {2, 8, 7, 1, 3, 5, 6, 4};
    input(arr, size);
    int result = findKStatistic(arr, 0, size-1, k);
    cout << result;

    delete [] arr;
    return 0;
}

int findMedianIndex(int* arr, int left, int right) {
    int middleElemKey = (left+right)/2;
    if( (arr[left] >= arr[right] && arr[left] <= arr[middleElemKey]) ||
        (arr[left] >= arr[middleElemKey] && arr[left] <= arr[right]) ) {
        return left;
    } else {
        if( arr[middleElemKey] >= arr[right] ) {
            return right;
        } else {
            return middleElemKey;
        }
    }
}

int partition(int* arr, int left, int right) {
    int pivotKey = findMedianIndex(arr, left, right);
    swap(arr[right], arr[pivotKey]);
    int i = left;
    int j = left;
    while( j < right ) {
        if( arr[j] >= arr[right] ) {
            j++;
        } else {
            if( j != i ) {
                swap(arr[j], arr[i]);
            }
            i++;
            j++;
        }
    }
    swap(arr[i], arr[right]);
    return i;
}

int findKStatistic(int* arr, int left, int right, int k) {
    if( k == 0 ) {
        return findMin(arr, right-left+1);
    }
    if( k == left-right ) {
        return findMax(arr, right-left+1);
    }
    int l = left;
    int r = right;
    int pivotPos = 0;
    do {
        pivotPos = partition(arr, l, r);
        if( k < pivotPos ) {
            r = pivotPos-1;
        } else {
            l = pivotPos+1;
        }
    } while( pivotPos != k );
    return arr[pivotPos];
}

void input(int* arr, int size) {
    for( int i = 0; i < size; i++ ) {
        cin >> arr[i];
    }
}

int findMax(int* arr, int size) {
    int maxKey = 0;
    for( int i = 1; i < size; i++ ) {
        if( arr[i] > arr[maxKey] ) {
            maxKey = i;
        }
    }
    return arr[maxKey];
}

int findMin(int* arr, int size) {
    int minKey = 0;
    for( int i = 1; i < size; i++ ) {
        if( arr[i] < arr[minKey] ) {
            minKey = i;
        }
    }
    return arr[minKey];
}

void output(int* arr, int size) {
    for( int i = 0; i < size; i++ ) {
        cout << arr[i] << " ";
    }
}