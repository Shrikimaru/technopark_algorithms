// Мазырин
// Роман
/*
4_1. Реализовать очередь с динамическим буфером.
*/
#include <iostream>

using namespace std;

const bool UNEXPECTED_VALUE = false;
const bool OK = true;
const int MEMORY = 3;

class Queue {

private:
    int* arr;
    int length;
    int num_of_elems;
    int head, tail;

    void grow_arr() {
        int* new_arr = new int[length*2];
        for( int i = head; i < length; i++ ) {
            new_arr[i-head] = arr[i];
        }
        for( int i = 0; i < tail; i++) {
            new_arr[length-tail+i] = arr[i];
        }
        head = 0;
        tail = length;
        length *= 2;
        delete [] arr;
        arr = new_arr;
    }

public:

    Queue() {
        length = MEMORY;
        arr = new int[length];
        head = 0;
        tail = 0;
        num_of_elems = 0;
    }

    void push_back(int val) {
        if( num_of_elems == length ) {
            grow_arr();
        };
        arr[tail++] = val;
        if(tail == length) {
            tail = 0;
        }
        num_of_elems++;
    }

    int pop_front() {
        if ( tail == head && num_of_elems != length ) {
            return -1;
        }
        int result = arr[head++];
        if( head == length ) {
            head = 0;
        }
        num_of_elems--;
        return result;
    }

    ~Queue() {
        delete [] arr;
    }
};

bool input(Queue& q) {
    int command;
    int e_value;

    cin >> command;
    cin >> e_value;

    switch(command) {

        case 2: {
            if( e_value != q.pop_front() ) {
                return UNEXPECTED_VALUE;
            }
            return OK;
        }

        case 3: {
            q.push_back(e_value);
            return OK;
        }

        default: {
            return OK;
        }
    }
}
int main() {
    Queue Q;

    int num_of_commands = 0;
    cin >> num_of_commands;

    bool flag = true;

    for( int i = 0; (i < num_of_commands) && flag; i++ ) {
        flag = input(Q);
    }

    if( flag ) {
        cout << "YES";
    } else {
        cout << "NO";
    }

    return 0;
}