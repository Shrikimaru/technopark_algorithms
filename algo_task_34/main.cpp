#include <iostream>
#include <cstdlib>

using namespace std;

struct AVLNode {

    AVLNode *left;
    AVLNode *right;

    int height;
    int val;

    AVLNode(int value): left(0), right(0), height(1), val(value) {}

    int getHeight() {
        return this == 0 ? 0:this->height;
    }

    int getBalanceFactor() {
        return this->left->getHeight() - this->right->getHeight();
    }

    void fixHeight() {
        this->height = max(this->left->getHeight(), this->right->getHeight()) + 1;
    }

};

class AVLTree {

    AVLNode* root;

    AVLNode* rotateLeft(AVLNode* node) {
        AVLNode *t = node->right;
        node->right = t->left;
        t->left = node;
        node->fixHeight();
        t->fixHeight();
        return t;
    }

    AVLNode* rotateRight(AVLNode* node) {
        AVLNode *t = node->left;
        node->left = t->right;
        t->right = node;
        node->fixHeight();
        t->fixHeight();
        return t;
    }

    AVLNode* balance(AVLNode*& node) {

        node->fixHeight();

        if( node->getBalanceFactor() == -2 ) {
            if( node->right->getBalanceFactor() == 1 ) {
                node->right = rotateRight(node->right);
            }
            return rotateLeft(node);
        }

        if( node->getBalanceFactor() == 2 ) {
            if( node->left->getBalanceFactor() == -1 ) {
                node->left = rotateLeft(node->left);
            }
            return rotateRight(node);
        }

        return node;
    }

    AVLNode* insertVal(AVLNode*& node, int pVal) {

        if( node == 0 ) {
            return new AVLNode(pVal);
        }

        if( pVal < node->val ) {
            node->left = insertVal(node->left, pVal);
        } else {
            node->right = insertVal(node->right, pVal);
        }

        return balance(node);
    }

    AVLNode* findMinNode(AVLNode* node) {
        if( node->left == 0 ) {
            return node;
        }
        return findMinNode(node->left);
    }

    AVLNode* deleteMinNode(AVLNode* node) {

        if( node->left == 0 ) {
            return node->right;
        }

        node->left = deleteMinNode(node->left);

        return balance(node);
    }

    AVLNode* removeVal(AVLNode* node, int pVal) {

        if( node == 0 ) {
            return 0;
        }

        if( node->val > pVal ) {
            node->left = removeVal(node->left, pVal);
        } else if( node->val < pVal ) {
            node->right = removeVal(node->right, pVal);
        }

        if( node->val == pVal ) {
            AVLNode* left = node->left;
            AVLNode* right = node->right;

            delete node;

            if( !right ) {
                return left;
            }

            AVLNode* min = findMinNode(right);

            min->right = deleteMinNode(right);
            min->left = left;

            return balance(min);
        }

        return balance(node);
    }

public:
    AVLTree() : root(0) {}

    int getTreeHeight() {
        return root->getHeight();
    }

    void addVal(int val) {
        root = insertVal(root, val);
    }

    void deleteVal(int val) {
        root = removeVal(root, val);
    }

    void deleteNode(AVLNode*& node) {
        if( node->left != 0 ) {
            deleteNode(node->left);
        }
        if( node->right != 0 ) {
            deleteNode(node->right);
        }
        delete node;
    }

    ~AVLTree() {
        if( root != 0 ) {
            deleteNode(root);
        }
    }
};

int main() {

    int val = 0;
    AVLTree* tree = new AVLTree();

    while( !cin.eof() ) {
        cin >> val;
        if( val < 0 ) {
            tree->deleteVal(abs(val));
        }
        if( val > 0 ) {
            tree->addVal(val);
        }
    }

    cout << tree->getTreeHeight();

    delete tree;

    return 0;
}