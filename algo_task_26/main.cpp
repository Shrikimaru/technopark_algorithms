// Мазырин
// Роман

/*
6_2. LSD для long long.
Дан массив неотрицательных целых 64-разрядных чисел.
Количество чисел не больше 10^6.
Отсортировать массив методом поразрядной сортировки LSD по байтам.
 */

#include <iostream>

using namespace std;
typedef unsigned long long int Lint;

void input(Lint* arr, int size);
void output(Lint* arr, int size);

void LSDSort(Lint* arr, int size, int byte);

int main() {
    int size = 0;
    cin >> size;
    Lint* arr = new Lint[size];
    input(arr, size);
    LSDSort(arr, size, 1);
    output(arr, size);
    return 0;
}

void input(Lint* arr, int size) {
    for( int i = 0; i < size; i++ ) {
        cin >> arr[i];
    }
}

void output(Lint* arr, int size) {
    for( int i = 0; i < size; i++ ) {
        cout << arr[i] << " ";
    }
}

void LSDSort(Lint* arr, int size, int byte) {
    int* digits = new int[256]
    for( int i = 0; i < 256; i++ ) {
        digits[i] = 0;
    }

    for( int i = 0; i < size; i++ ) {
        unsigned char digit = (arr[i]>>((byte-1)*8)) & 255;
        digits[digit]++;
    }

    for( int i = 1; i < 256; i++ ) {
        digits[i] += digits[i-1];
    }

    Lint* tempData = new Lint[size];

    for( int i = size-1; i >= 0; i-- ) {
        unsigned char digit = (arr[i]>>((byte-1)*8)) & 255;
        digits[digit]--;
        tempData[digits[digit]] = arr[i];
    }

    for( int i = 0; i < size; i++ ) {
        arr[i] = tempData[i];
    }

    delete [] digits;
    delete [] tempData;

    if( byte < 8 ) {
        LSDSort(arr, size, byte+1);
    }
}