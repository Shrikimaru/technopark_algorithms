// Мазырин
// Роман

/* Дано натуральное число N. Представить N в виде A + B, так,
   что НОД(A, B) максимален, A <= B. Вывести A и B
*/

#include <iostream>
#include "assert.h"

using namespace std;

int findMaxNODOfSum( int, int&, int& );

int main() {

    int num = 0;

    cin >> num;
    assert( num > 1 );

    int s1 = num-1;
    int s2 = 1;

    findMaxNODOfSum( num, s1, s2);
    if( s1 >= s2 ) {
        cout << s2 << " " << s1 << endl;
    } else {
        cout << s1 << " " << s2 << endl;
    }
    return 0;
}

int findMaxNODOfSum( int num, int& s1, int& s2 ) {

    int NODOfS = 1;

    for( int i = 1; i <= num/2; i++ ) {
        if( (num-i)%i == 0 && (i > NODOfS) ) {
            s1 = i;
            s2 = num-i;
            NODOfS = i;
        }
    }
}