// Мазырин
// Роман
/*
 Реализуйте структуру данных типа “множество строк” на основе динамической хеш-таблицы с открытой адресацией. Хранимые строки непустые и состоят из строчных латинских букв. Начальный размер таблицы должен быть равным 8-ми. Перехеширование выполняйте в случае, когда коэффициент заполнения таблицы достигает 3/4.
Структура данных должна поддерживать операции добавления строки в множество, удаления строки из множества и проверки принадлежности данной строки множеству.
1_1. Для разрешения коллизий используйте квадратичное пробирование. i-ая проба
g(k, i)=g(k, i-1) + i (mod m). m - степень двойки.

Формат входных данных
Каждая строка входных данных задает одну операцию над множеством. Запись операции состоит из типа операции и следующей за ним через пробел строки, над которой проводится операция.
Тип операции  – один из трех символов:
    +  означает добавление данной строки в множество; 
    -  означает удаление  строки из множества;  
    ?  означает проверку принадлежности данной строки множеству. 
При добавлении элемента в множество НЕ ГАРАНТИРУЕТСЯ, что он отсутствует в этом множестве. При удалении элемента из множества НЕ ГАРАНТИРУЕТСЯ, что он присутствует в этом множестве.
Формат выходных данных
Программа должна вывести для каждой операции одну из двух строк OK или FAIL, в зависимости от того, встречается ли данное слово в нашем множестве.
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

const int BEGIN_SIZE = 8;
const char* DELETED_VAL = "DELETED12345qwerty";

int myHash(const string& data, int tableSize) {
    int hash = 0;
    for( unsigned int i = 0; i < data.size(); i++ ) {
        hash = (hash * 1483 + data[i]) % tableSize;
    }
    return hash;
}

class HashTable {
public:
    int numOfElems;
    vector<string>* arr;
    HashTable();
    ~HashTable();
    HashTable(int size);
    bool addValue(const string& val);
    int hasValue(const string& val);
    bool deleteValue(const string& val);
    float countKoeff();
    int getPos(const string& k, int i, int prevPos);
    void reBuild(int type);
};

HashTable::HashTable(){
    this->arr = new vector<string>;
    for( register unsigned int i = 0; i < BEGIN_SIZE; i++ ) {
        arr->push_back("");
    }
    numOfElems = 0;
}

HashTable::HashTable(int size) {
    this->arr = new vector<string>;
    for( register unsigned int i = 0; i < size; i++ ) {
        arr->push_back("");
    }
    numOfElems = 0;
}

HashTable::~HashTable() {
    delete arr;
}

float HashTable::countKoeff(){
    if( arr->size() == 0 ) {
        return 0;
    }
    return numOfElems/arr->size();
}

int HashTable::getPos(const string& k, int i, int prevPos){
    if( i == 0 ) {
        return myHash(k, (int)arr->size());
    } else {
        return (int)((prevPos + (i % arr->size())) % arr->size());
    }
}

bool HashTable::addValue(const string& val) {
    if( this->hasValue(val) != -1 ) {
        return false;
    } else {
        int key = 0;
        for( register unsigned int i = 0; i < arr->size(); i++ ) {
            key = getPos(val, i, key);
            if( (*arr)[key] == "" || (*arr)[key] == DELETED_VAL ) {
                (*arr)[key] = val;
                numOfElems++;
                if( this->countKoeff() > 0.75 ) {
                    reBuild(1);
                }
                return true;
            }
        }
        return false;
    }
}

int HashTable::hasValue(const string& val) {
    int key = 0;
    for( register unsigned int i = 0; i < arr->size(); i++ ) {
        key = getPos(val, i, key);
        if( (*arr)[key] == val ) {
            return key;
        }
        if( (*arr)[key] == "" ) {
            return -1;
        }
    }
    return -1;
}

bool HashTable::deleteValue(const string& val) {
    int valKey = this->hasValue(val);
    if( valKey == -1 ) {
        return false;
    } else {
        (*arr)[valKey] = DELETED_VAL;
        numOfElems--;
//        if( this->countKoeff() < 0.75 ) {
//            reBuild(-1);
//        }
        return true;
    }
}

void HashTable::reBuild( int type ) {
    long int newSize = 0;
    if( type == 1 ) {
        newSize = arr->size()*2;
    } else {
        newSize = arr->size()/2;
    }
    vector<string>* newVect = new vector<string>;
    vector<string>* temp = arr;
    this->arr = newVect;
    for( register unsigned int i = 0; i < newSize; i++ ) {
        arr->push_back("");
    }
    for( register unsigned int i = 0; i < temp->size(); i++ ) {
        if( (*temp)[i] != "" && (*temp)[i] != DELETED_VAL ) {
            this->addValue((*temp)[i]);
        }
    }
    delete temp;
}

bool doAction(HashTable* t) {
    char action = '+';
    string str = "";
    bool isOk = true;
    cin >> action;
    cin >> str;
    if( cin.eof() ) {
        return false;
    }
    switch(action) {
        case '+': isOk = t->addValue(str); break;
        case '-': isOk = t->deleteValue(str); break;
        case '?': isOk = (t->hasValue(str) != -1); break;
    }
    cout << (isOk ? "OK" : "FAIL") << endl;
    return true;
}

int main() {
    HashTable* t = new HashTable();
    while( doAction(t) ) {}
    delete t;
    return 0;
}