// Мазырин
// Роман
/*
На числовой прямой окрасили N отрезков.
Известны координаты левого и правого концов каждого отрезка (Li и Ri).
Найти сумму длин частей числовой прямой, окрашенных ровно в один слой.
 */

#include <iostream>

using namespace std;

class PointOfLine {
public:
    double x;
    bool isStart;
    PointOfLine(){};
    PointOfLine(double px, bool pIsStart) : x(px), isStart(pIsStart) {}
    bool operator>(PointOfLine& point) {
        return (this->x > point.x);
    }
    bool operator<(PointOfLine& point) {
        return (this->x < point.x);
    }
    bool operator==(PointOfLine& point) {
        return (this->x == point.x);
    }
};

class Line {
public:
    double begin;
    double end;
    Line(){}
    Line(double b, double e) : begin(b), end(e) {}
};

void input(Line*, int);
int determine(Line*, int);

template<class T>
void arrOutput(T* arr, int size);

template<class T>
void mergeSort(T*, int, int);

template<class T>
void merge(T*, int, int, int);

ostream& operator<<(ostream& stream, PointOfLine& point) {
    stream << point.x;
    return stream;
}



int main() {
    int numOfLines = 0;
    cin >> numOfLines;

    Line* arr = new Line[numOfLines]();
    input(arr, numOfLines);
    int res = determine(arr, numOfLines);
    delete [] arr;
    cout << res;
    return 0;
}




void input(Line* arr, int size) {
    int begin = 0;
    int end = 0;
    for( int i = 0; i < size; i++ ) {
        cin >> begin;
        cin >> end;
        arr[i] = Line(begin, end);
    }
}

template<class T>
void merge(T* arr, int left, int split, int right) {
    T* buf = new T[right-left+1];
    int l = left, r = split+1, curKey = 0;
    while( l <= split && r <= right ) {
        if( arr[l] < arr[r] ) {
            buf[curKey++] = arr[l++];
        } else {
            buf[curKey++] = arr[r++];
        }
    }

    if( l > split ) {
        for( ; r <= right; r++ ) {
            buf[curKey++] = arr[r];
        }
    } else {
        for( ; l <= split; l++ ) {
            buf[curKey++] = arr[l];
        }
    }

    for( int i = left; i <= right; i++ ) {
        arr[i] = buf[i-left];
    }

    delete [] buf;
}

template<class T>
void mergeSort(T* arr, int left, int right) {
    if( left < right ) {
        int split = (left+right)/2;
        mergeSort(arr, left, split);
        mergeSort(arr, split+1, right);
        merge(arr, left, split, right);
    }
}

int determine(Line* lines, int numOfLines) {
    int numOfPoints = numOfLines*2;
    PointOfLine* points = new PointOfLine[numOfPoints];
    for( int i = 0; i < numOfLines; i++ ) {
        double begin = lines[i].begin;
        double end = lines[i].end;
        points[i*2] = PointOfLine(begin, true);
        points[i*2+1] = PointOfLine(end, false);
    }

    mergeSort(points, 0, numOfPoints-1);

    int resLength = 0;
    int numOfLayers = 0;
    for( int i = 0; i < numOfPoints; i++ ) {
        if( points[i].isStart ) {
            numOfLayers++;
        } else {
            numOfLayers--;
        }
        if( ( points[i].isStart && numOfLayers-1 == 1) ||
            (!points[i].isStart && numOfLayers   == 0) ) {
            resLength += (points[i].x - points[i-1].x);
        }
    }

    delete [] points;
    return resLength;
}

template<class T>
void arrOutput(T* arr, int size) {
    for( int i = 0; i < size; i++ ) {
        cout << arr[i] << endl;
    }
}