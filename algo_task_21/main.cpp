// Мазырин
// Роман

/*
Во всех вариантах данной задачи необходимо реализовать указанный алгоритм сортировки массива целых чисел.
Количество чисел в массиве определяется окончанием стандартного потока ввода и заранее не известно.
Сортировка вставками.
*/

#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

void insertionSort( int* arr, int size );
void chooseSort( int* arr, int size);
void bubbleSort( int* arr, int size);

int main() {
    vector<int> arr;
    do{
        int temp = 0;
        if( scanf("%d", &temp) != 1 ) {
            break;
        }
        arr.push_back(temp);
    } while( true );

    insertionSort( &arr[0], arr.size() );
//    bubbleSort( &arr[0], arr.size() );
//    chooseSort( &arr[0], arr.size() );

    for( int i = 0; i < arr.size(); i++ ) {
        cout << arr[i];
        if( i+1 != arr.size() ) {
            cout << endl;
        }
    }

    return 0;
}

void insertionSort( int* arr, int size ) {
    for( int i = 1; i < size; i++ ) {
        for( int j = i-1; j >= 0 && arr[j] > arr[j+1]; j-- ) {
            swap(arr[j], arr[j+1]);
        }
    }
}

void chooseSort( int* arr, int size ) {
    for( int i = 0; i < size - 1; i++ ) {
        int curMinKey = i;
        for( int j = i+1; j < size; j++ ) {
            if( arr[j] < arr[curMinKey] ) {
                curMinKey = j;
            }
        }
        swap( arr[i], arr[curMinKey] );
    }
}

void bubbleSort( int* arr, int size ) {
    for( int i = 0; i < size; i++ ) {
        for( int j = 0; j < size - 1 - i; j++ ) {
            if( arr[j] > arr[j+1] ) {
                swap( arr[j], arr[j+1] );
            }
        }
    }
}