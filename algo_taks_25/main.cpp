// Мазырин
// Роман
/*
Дана последовательность целых чисел a1...an и натуральное число k, такое что для любых i, j:
если j >= i + k, то a[i] <= a[j].
Требуется отсортировать последовательность. Последовательность может быть очень длинной.
Время работы O(n * log(k)). Доп. память O(k). Использовать слияние.
 */

#include <iostream>

using namespace std;

void input(int*, int);
void output(int*, int);
void merge(int*, int, int, int);
void specialMergeSort(int*, int, int, int);

int main() {
    int size = 5;
    int k = 5;
    cin >> size;
    cin >> k;

    int* arr = new int[size];

    input(arr, size);
//    int arr[5] = {0, 4, 3, 2, 1};
    specialMergeSort(arr, 0, size-1, k);
    output(arr, size);

    delete [] arr;
    return 0;
}

void input(int* arr, int size) {
    for( int i = 0; i < size; i++ ) {
        cin >> arr[i];
    }
}

void output(int* arr, int size) {
    for( int i = 0; i < size; i++ ) {
        cout << arr[i];
        if( i+1 != size ) {
           cout << " ";
        }
    }
}

void merge(int* arr, int left, int split, int right) {
    int* buf = new int[right-left+1];
    int l = left, r = split+1, curKey = 0;
    while( l <= split && r <= right ) {
        if( arr[l] < arr[r] ) {
            buf[curKey++] = arr[l++];
        } else {
            buf[curKey++] = arr[r++];
        }
    }

    if( l > split ) {
        for( ; r <= right; r++ ) {
            buf[curKey++] = arr[r];
        }
    } else {
        for( ; l <= split; l++ ) {
            buf[curKey++] = arr[l];
        }
    }

    for( int i = left; i <= right; i++ ) {
        arr[i] = buf[i-left];
    }

    delete [] buf;
}

void mergeSort(int* arr, int left, int right) {
    if( left < right ) {
        int split = (left+right)/2;
        mergeSort(arr, left, split);
        mergeSort(arr, split+1, right);
        merge(arr, left, split, right);
    }
}

void specialMergeSort(int* arr, int left, int right, int k) {
    bool eoArr = false;

    int rightCurrentBoundary = left+k*2-1;
    if( rightCurrentBoundary > right ) {
        rightCurrentBoundary = right;
        eoArr = true;
    }
    mergeSort(arr, left, rightCurrentBoundary);

    for( int i = rightCurrentBoundary+1; !eoArr; i += k ) {
        rightCurrentBoundary = i+k-1;
        if( rightCurrentBoundary > right ) {
            rightCurrentBoundary = right;
            eoArr = true;
        }
        mergeSort(arr, i, rightCurrentBoundary);
        merge(arr, i-k, i-1, rightCurrentBoundary);
    }
}